# CHANGELOG.md

## Unreleased

New features

- PaNET ontology parsing for techniques.
- API for adding scan metadata (HDF5) and dataset metadata (ICAT+) in Bliss.
